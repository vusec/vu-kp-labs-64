#include <inc/assert.h>
#include <inc/stdio.h>

#include <inc/x86-64/asm.h>
#include <inc/x86-64/gdt.h>
#include <inc/x86-64/idt.h>

#include <kern/env.h>
#include <kern/idt.h>
#include <kern/monitor.h>
#include <kern/sched.h>
#include <kern/spinlock.h>
#include <kern/syscall.h>

static const char *int_names[256] = {
    [INT_DIVIDE] = "Divide-by-Zero Error Exception (#DE)",
    [INT_DEBUG] = "Debug (#DB)",
    [INT_NMI] = "Non-Maskable Interrupt",
    [INT_BREAK] = "Breakpoint (#BP)",
    [INT_OVERFLOW] = "Overflow (#OF)",
    [INT_BOUND] = "Bound Range (#BR)",
    [INT_INVALID_OP] = "Invalid Opcode (#UD)",
    [INT_DEVICE] = "Device Not Available (#NM)",
    [INT_DOUBLE_FAULT] = "Double Fault (#DF)",
    [INT_TSS] = "Invalid TSS (#TS)",
    [INT_NO_SEG_PRESENT] = "Segment Not Present (#NP)",
    [INT_SS] = "Stack (#SS)",
    [INT_GPF] = "General Protection (#GP)",
    [INT_PAGE_FAULT] = "Page Fault (#PF)",
    [INT_FPU] = "x86 FPU Floating-Point (#MF)",
    [INT_ALIGNMENT] = "Alignment Check (#AC)",
    [INT_MCE] = "Machine Check (#MC)",
    [INT_SIMD] = "SIMD Floating-Point (#XF)",
    [INT_SECURITY] = "Security (#SX)",
    [IRQ_OFFSET...IRQ_OFFSET+16] = "Hardware interrupt",
    [INT_SYSCALL] = "System call",
};

static struct idt_entry entries[256];
static struct idtr idtr = {
    .limit = sizeof(entries) - 1,
    .entries = entries,
};

static const char *get_int_name(unsigned int_no)
{
    if (!int_names[int_no])
        return "Unknown Interrupt";

    return int_names[int_no];
}

void print_int_frame(struct int_frame *frame)
{
    cprintf("INT frame at %p from CPU %d\n", frame, cpunum());

    /* Print the interrupt number and the name. */
    cprintf(" INT %u: %s\n",
        frame->int_no,
        get_int_name(frame->int_no));

    /* Print the error code. */
    switch (frame->int_no) {
    case INT_PAGE_FAULT:
        cprintf(" CR2 %p\n", read_cr2());
        cprintf(" ERR 0x%016llx (%s, %s, %s)\n",
                frame->err_code,
                frame->err_code & 4 ? "user" : "kernel",
                frame->err_code & 2 ? "write" : "read",
                frame->err_code & 1 ? "protection" : "not present");
        break;
    default:
        cprintf(" ERR 0x%016llx\n", frame->err_code);
    }

    /* Print the general-purpose registers. */
    cprintf(" RAX 0x%016llx"
            " RCX 0x%016llx"
            " RDX 0x%016llx"
            " RBX 0x%016llx\n"
            " RSP 0x%016llx"
            " RBP 0x%016llx"
            " RSI 0x%016llx"
            " RDI 0x%016llx\n"
            " R8  0x%016llx"
            " R9  0x%016llx"
            " R10 0x%016llx"
            " R11 0x%016llx\n"
            " R12 0x%016llx"
            " R13 0x%016llx"
            " R14 0x%016llx"
            " R15 0x%016llx\n",
            frame->rax, frame->rcx, frame->rdx, frame->rbx,
            frame->rsp, frame->rbp, frame->rsi, frame->rdi,
            frame->r8,  frame->r9,  frame->r10, frame->r11,
            frame->r12, frame->r13, frame->r14, frame->r15);

    /* Print the IP, segment selectors and the RFLAGS register. */
    cprintf(" RIP 0x%016llx"
            " RFL 0x%016llx\n"
            " CS  0x%04x"
            "            "
            " DS  0x%04x"
            "            "
            " SS  0x%04x\n",
            frame->rip, frame->rflags,
            frame->cs, frame->ds, frame->ss);
}

void idt_init(void)
{
    /* Set up the interrupt handlers in the IDT. */

    /* LAB 3: your code here. */

    idt_init_percpu();
}

void idt_init_percpu(void)
{
    /* Load the IDT. */
    load_idt(&idtr);
}

void int_dispatch(struct int_frame *frame)
{
    /* Handle processor exceptions. */
    /* LAB 3: your code here. */
    switch (frame->int_no) {
    case IRQ_TIMER:
        /* Handle clock interrupts. Don't forget to acknowledge the interrupt
         * using lapic_eoi() before calling the scheduler!
         */
        break;
    case IRQ_SPURIOUS:
        cprintf("Spurious interrupt on IRQ #7.\n");
        print_int_frame(frame);
        return;
    default: break;
    }

    /* Unexpected trap: The user process or the kernel has a bug. */
    print_int_frame(frame);

    if (frame->cs == GDT_KCODE) {
        panic("unhandled interrupt in kernel");
    } else {
        env_destroy(curenv);
        return;
    }
}

void int_handler(struct int_frame *frame)
{
    /* The environment may have set DF and some versions of GCC rely on DF being
     * clear. */
    asm volatile("cld" ::: "cc");

    /* Halt the CPU if some other CPU has called panic(). */
    extern char *panicstr;

    if (panicstr)
        asm volatile("hlt");

    /* Re-acqurie the big kernel lock if we were halted in sched_yield(). */
    if (xchg(&thiscpu->cpu_status, CPU_STARTED) == CPU_HALTED)
        lock_kernel();

    /* Check that interrupts are disabled.
     * If this assertion fails, DO NOT be tempted to fix it by inserting a "cli"
     * in the interrupt path.
     */
    assert(!(read_rflags() & FLAGS_IF));

    cprintf("Incoming INT frame at %p\n", frame);

    if ((frame->cs & 3) == 3) {
        /* Interrupt from user mode. */
        /* Acquire the big kernel lock before doing any serious kernel work.
         * LAB 6: your code here. */
        assert(curenv);

        /* Garbage collect if current enviroment is a zombie. */
        if (curenv->env_status == ENV_DYING) {
            env_free(curenv);
            curenv = NULL;
            sched_yield();
        }

        /* Copy interrupt frame (which is currently on the stack) into
         * 'curenv->env_frame', so that running the environment will restart at
         * the point of interrupt. */
        curenv->env_frame = *frame;

        /* Avoid using the frame on the stack. */
        frame = &curenv->env_frame;
    }

    /* Dispatch based on the type of interrupt that occurred. */
    int_dispatch(frame);

    /* If we made it to this point, then no other environment was scheduled, so
     * we should return to the current environment if doing so makes sense. */
    if (curenv && curenv->env_status == ENV_RUNNING)
        env_run(curenv);
    else
        sched_yield();
}

void page_fault_handler(struct int_frame *frame)
{
    void *fault_va;

    /* Read the CR2 register to find the faulting address. */
    fault_va = read_cr2();

    /* Handle kernel-mode page faults. */
    /* LAB 3: your code here. */

    /* We have already handled kernel-mode exceptions, so if we get here, the
     * page fault has happened in user mode.
     */

    /* Destroy the environment that caused the fault. */
    cprintf("[%08x] user fault va %p ip %p\n",
        curenv->env_id, fault_va, frame->rip);
    print_int_frame(frame);
    env_destroy(curenv);
}

